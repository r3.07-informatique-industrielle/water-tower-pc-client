#include <stdio.h>
#include <time.h>

unsigned long millis()
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	return ((unsigned long)now.tv_sec) * 1000 + ((unsigned long)now.tv_nsec) / 1000000;
}

/* ********************************************************
 *  TEMPORISATION RETARD A LA MONTEE                      *
 *  La sortie passe à 1 au bout de la tempo               *
 ******************************************************** */

class TemporisationRetardMontee
{
    // methodes
  public :
    // Contructeur qui prend la duree souhaitee de la temporisation
	TemporisationRetardMontee(unsigned long duree)
	{
		this->duree = duree;
		sortie = false;
		captureTemps = false;
	}
    // Activation de la temporisation. Doit etre fait tout le temps de la duree de la tempo
	void activation()
	{
		// Capture du temps de reference
		if(!captureTemps)
		{
			debut = millis();
			captureTemps = true;
		}
		// Calcul du temps ecoule depuis le temps de reference
		tempsEcoule = millis() - debut;
		// Mise a 1 de la fin de tempo
		if (tempsEcoule >= duree)
		{
			sortie = true;
			captureTemps = false;
		}
		else
		{
			sortie = false;
		}
	}
	// Precharge de la temporisation
	void setDuree(unsigned long majduree)
	{
		duree = majduree;
		sortie = false;
		captureTemps = false;
	}
	// Interrogation du bit de fin de tempo
    bool getSortie()
	{
		return(sortie);
	}
	// Recuperation du temps ecoule depuis le debut si necessaire
	unsigned long getTempsEcoule()
	{
		return(tempsEcoule);
	}

    // Attributs
  private:
    unsigned long duree;
    unsigned long debut;
    unsigned long tempsEcoule;
		bool captureTemps;
    bool sortie;
};

/********************************************************
****  TEMPORISATION RETARD A LA DESCENTE ******************
*  La sortie passe à 0 au bout de la tempo **************
*********************************************************/
class TemporisationRetardDescente
{
    // methodes
  public :
    // Contructeur qui prend la duree souhaitee de la temporisation
	TemporisationRetardDescente(unsigned long duree)
	{
		this->duree = duree;
		sortie = false;
		captureTemps = false;
	}
    // Activation de la temporisation. Doit etre fait tout le temps de la duree de la tempo
	void activation()
	{
		// Capture du temps de reference
		if(!captureTemps)
		{
			debut = millis();
			captureTemps = true;
			sortie = true;
		}
		// Calcul du temps ecoule depuis le temps de reference
		tempsEcoule = millis() - debut;
		// Mise a 0 de la fin de tempo
		if (tempsEcoule >= duree)
		{
			sortie = false;
			captureTemps = false;
		}
	}
	// Precharge de la temporisation
	void setDuree(unsigned long majduree)
	{
		duree = majduree;
		sortie = false;
		captureTemps = false;
	}
	// Interrogration du bit de fin de tempo
    bool getSortie()
	{
		return(sortie);
	}
	// Recuperation du temps ecoule depuis le debut si necessaire
    unsigned long getTempsEcoule()
	{
		return(tempsEcoule);
	}

    // Attributs
  private:
    unsigned long duree;
    unsigned long debut;
    unsigned long tempsEcoule;
    bool captureTemps;
    bool sortie;
};

/********************************************************
****  CLIGNOTEUR *************************************
*********************************************************/
class Clignoteur
{
    // methodes
  public :
	// Construteur qui prend en parametre le temps haut ou bas souhaitee
	Clignoteur(int baseDeTemps)
	{
		this->baseDeTemps = baseDeTemps;
	}
	// Fonction qui renvoie true si le clignoteur est à l'état haut et false s'il est à l'état bas
    bool statut()
	{
		return ((millis() / baseDeTemps) % 2 == 1);
	}

    // Attributs
  private:
    int baseDeTemps;
};

/********************************************************
****  COMPTEUR *************************************
**** ATTENTION : Il faut gerer le front montant dans le programme
*********************************************************/
class Compteur
{
    // methodes
  public :
	// Constructeur qui prend en parametre la valeur de preselection
	Compteur(int valeurPreselection)
	{
		this->valeurPreselection = valeurPreselection;
		valeur = 0;
	}
	// Incrementation du compteur
	void incrementer()
	{
		valeur++;
	}
	// Decrementation du compteur
	void decrementer()
	{
		valeur--;
	}
	// remise a zero du compteur
	void remettreAZero()
	{
		valeur = 0;
	}
	// recuperation de la valeur courante
	int getValeurCourante()
	{
		return(valeur);
	}
	// est-ce que la preselection est atteinte (sortie Q compteur Siemens ou Schnieder)
	bool getSortie()
	{
		return(valeur == valeurPreselection);
	}

    // Attributs
  private:
    int valeur;
    int valeurPreselection;
};

/********************************************************
****  MISE A L'ECHELLE DE VALEUR ************************
*********************************************************/
class MiseAEchelle
{
    // methodes
  public :
	// Constructeur qui ne prend en parametre la plage d'entree et la plage de sortie
	MiseAEchelle(float minEntree,float maxEntree,float minSortie,float maxSortie)
	{
		this->minEntree = minEntree;
		this->maxEntree = maxEntree;
		this->minSortie = minSortie;
		this->maxSortie = maxSortie;
	}
	// fonction de conversion qui prend la valeur a convertir et renvoie la valeur convertie
	float convertir(float valeurAConvertir)
	{
		if(valeurAConvertir >= minEntree && valeurAConvertir <= maxEntree)
		{
			float norm = (1 / (maxEntree - minEntree)) * (valeurAConvertir - minEntree);
			float scale = (maxSortie - minSortie) * norm + minSortie;
			return(scale);
		}
		return(-1000);
	}

    // Attributs
  private:
    float minEntree;
    float minSortie;
    float maxEntree;
		float maxSortie;
};
