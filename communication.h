#include <windows.h>

#define OP_DIGITAL_READ 0
#define OP_DIGITAL_WRITE 1
#define OP_ANALOG_READ 2
#define OP_ANALOG_WRITE 3
#define OP_PIN_MODE 4

void CommunicationOpen();
void CommunicationClose();

void digitalWrite(int pin, int value);
void analogWrite(int pin, float value);
int digitalRead(int pin);
float analogRead(int pin);
