#include <windows.h>
#include <stdio.h>

#include "communication.h"


#define IO_INPUT 1
#define IO_OUTPUT 2

#define OUT_BEEP 254
#define OUT_END 255

typedef struct
{
  int mode;
  int registre;
  int dvalue;
  float fvalue;
} datastruct;

HANDLE hPipe;

void CommunicationOpen()
{
  LPTSTR lpszPipename = strdup("\\\\.\\pipe\\geii");

  // Try to open a named pipe; wait for it, if necessary.
  while (1)
  {
    hPipe = CreateFile(
        lpszPipename, // pipe name
        GENERIC_READ | GENERIC_WRITE,
        0,             // no sharing
        NULL,          // default security attributes
        OPEN_EXISTING, // opens existing pipe
        0,             // default attributes
        NULL);         // no template file

    // Break if the pipe handle is valid.
    if (hPipe != INVALID_HANDLE_VALUE)
    {
      break;
    }

    // Exit if an error other than ERROR_PIPE_BUSY occurs.
    if (GetLastError() != ERROR_PIPE_BUSY)
    {
      printf("Could not open pipe. GLE=%lu\n", GetLastError());
      return;
    }

    // All pipe instances are busy, so wait for 20 seconds.
    if (!WaitNamedPipe(lpszPipename, 20000))
    {
      printf("Could not open pipe: 20 second wait timed out.");
      return;
    }
  }

  // The pipe connected; change to message-read mode.
  DWORD dwMode = PIPE_READMODE_BYTE;
  BOOL fSuccess = SetNamedPipeHandleState(
      hPipe,   // pipe handle
      &dwMode, // new pipe mode
      NULL,    // don't set maximum bytes
      NULL);   // don't set maximum time
  if (!fSuccess)
  {
    printf("SetNamedPipeHandleState failed. GLE=%lu\n", GetLastError());
    return;
  }
}

void CommunicationClose() {
  CloseHandle(hPipe);
}

/* ** WRITE ** */

void digitalWrite(int pin, int value)
{
  DWORD cbWritten;

  datastruct data;
  data.mode = OP_DIGITAL_WRITE;
  data.dvalue = value;
  data.registre = pin;

  BOOL fSuccess = WriteFile(hPipe, &data, sizeof(datastruct), &cbWritten, NULL);

  if (!fSuccess)
  {
    printf("Erreur lors de l'ecriture d'une donnee. %lu\n", GetLastError());
  }
}

void analogWrite(int pin, float value)
{
  DWORD cbWritten;

  datastruct data;
  data.mode = OP_ANALOG_WRITE;
  data.fvalue = value;
  data.registre = pin;

  BOOL fSuccess = WriteFile(hPipe, &data, sizeof(datastruct), &cbWritten, NULL);

  if (!fSuccess)
  {
    printf("Erreur lors de l'�criture d'une donn�e. %lu\n", GetLastError());
  }
}

/* ** READ ** */

int digitalRead(int pin)
{
  BOOL fSuccess;
  DWORD cbWritten;
  DWORD cbRead;

  datastruct data;
  data.mode = OP_DIGITAL_READ;
  data.dvalue = -1;
  data.registre = pin;

  fSuccess = WriteFile(hPipe, &data, sizeof(datastruct), &cbWritten, NULL);

  do
  {
    fSuccess = ReadFile(hPipe, &data, sizeof(data), &cbRead, NULL);
    if (!fSuccess && GetLastError() != ERROR_MORE_DATA)
      break;
  } while (!fSuccess); // repeat loop if ERROR_MORE_DATA

  if (!fSuccess)
  {
    printf("Erreur lors de la lecture d'une donn\201e. %lu\n", GetLastError());
    // Die
  }

  return data.dvalue;
}

float analogRead(int pin)
{
  BOOL fSuccess;
  DWORD cbWritten;
  DWORD cbRead;

  datastruct data;
  data.mode = OP_ANALOG_READ;
  data.fvalue = -1;
  data.registre = pin;

  fSuccess = WriteFile(hPipe, &data, sizeof(datastruct), &cbWritten, NULL);

  do
  {
    fSuccess = ReadFile(hPipe, &data, sizeof(data), &cbRead, NULL);

    //printf("%d", fSuccess);

    if (!fSuccess && GetLastError() != ERROR_MORE_DATA)
      break;

  } while (!fSuccess); // repeat loop if ERROR_MORE_DATA

  if (!fSuccess)
  {
    printf("Erreur lors de la lecture d'une donn�e. %lu\n", GetLastError());
  }

  return data.fvalue;
}
