/* **************************************************
 * VOTRE NOM : ???????????
 * ZONE PROT�E : NE PAS �CRIRE ICI !
 *
 * **************************************************/
#include <stdio.h>
#include <conio.h>

#include "main.h"
#include "pin.h"
#include "communication.h"
#include "AutomForArduino.cpp"

int main()
{
  CommunicationOpen();

  init();

  while (1)
  {
    loop();

    if (GetAsyncKeyState(VK_ESCAPE) & 0x01)
    {
      break;
    }

    Sleep(100);
  }

  CommunicationClose();

  return 0;
}

/* **************************************************
 * R�PONDRE CI-DESSOUS SEULEMENT !
 * Les m�thodes imitent le fonctionnement du syst�me Arduino
 * void init : Appel�e une fois au d�but de l'ex�cution
 * void loop : Appel�e toutes les 200ms
 * **************************************************/

// Ecrire les variables globales ici

int etape = 0;
int bp_keyboard[4], bp_keyboard_a[4], bp_keyboard_fm[4];
int bp_mode, bp_mode_fm;
int pompe1, pompe2, pompe3, pompe4;
int sensor_max, sensor_high, sensor_low, sensor_min;

TemporisationRetardMontee tempo(3000);

void init()
{
}

void loop()
{
  LireEntree();
  EvolutionGrafcet();
  Actions();
}

void LireEntree()
{
  int input;
  input = digitalRead(IN_KEYBOARD_1);
  bp_keyboard_fm[0] = (input > bp_keyboard[0]);
  bp_keyboard[0] = input;

  input = digitalRead(IN_KEYBOARD_2);
  bp_keyboard_fm[1] = (input > bp_keyboard[1]);
  bp_keyboard[1] = input;

  input = digitalRead(IN_KEYBOARD_3);
  bp_keyboard_fm[2] = (input > bp_keyboard[2]);
  bp_keyboard[2] = input;

  input = digitalRead(IN_KEYBOARD_4);
  bp_keyboard_fm[3] = (input > bp_keyboard[3]);
  bp_keyboard[3] = input;

  input = digitalRead(IN_KEYBOARD_A);
  bp_mode_fm = (input > bp_mode);
  bp_mode = input;

  sensor_min = digitalRead(IN_SENSOR_MIN);
  sensor_low = digitalRead(IN_SENSOR_LOW);
  sensor_high = digitalRead(IN_SENSOR_HIGH);
  sensor_max = digitalRead(IN_SENSOR_MAX);
}

void EvolutionGrafcet()
{
  int etape_futur = etape;

  if (etape < 10 && bp_mode_fm)
  {
    printf("Mode automatique activ�.\n");
    etape_futur = 10;
    pompe1 = pompe2 = pompe3 = pompe4 = 0;
  }

  if (etape <= 2 && bp_keyboard_fm[0])
  {
    pompe1 = !pompe1;
  }

  if (etape <= 2 && bp_keyboard_fm[1])
  {
    pompe2 = !pompe2;
  }

  if (etape <= 2 && bp_keyboard_fm[2])
  {
    pompe3 = !pompe3;
  }

  if (etape <= 2 && bp_keyboard_fm[3])
  {
    pompe4 = !pompe4;
  }

  if (etape == 0 && !sensor_min)
  {
    etape_futur = 1;
  }

  if (etape == 1)
  {
    etape_futur = 2;
  }

  if (etape == 2 && sensor_min)
  {
    etape_futur = 0;
  }

  if (etape >= 10 && bp_mode_fm)
  {
    printf("Mode manuel activ�.\n");
    etape_futur = 0;
    pompe1 = pompe2 = pompe3 = pompe4 = 0;
  }

  if (sensor_max)
  {
    pompe1 = pompe2 = pompe3 = pompe4 = 0;
  }

  /* Automatique */
  if (etape == 10 && !sensor_low && !sensor_high)
  {
    etape_futur = 11;
  }

  if (etape == 11 && sensor_high)
  {
    etape_futur = 10;
  }

  if (etape == 11 && tempo.getSortie())
  {
    etape_futur = 12;
  }

  if (etape == 12 && sensor_high)
  {
    etape_futur = 13;
  }

  if (etape == 12 && tempo.getSortie())
  {
    etape_futur = 14;
  }

  if (etape == 13 && tempo.getSortie())
  {
    etape_futur = 10;
  }

  if (etape == 13 && !sensor_low && !sensor_high)
  {
    etape_futur = 12;
  }

  if (etape == 14 && sensor_high)
  {
    etape_futur = 15;
  }

  if (etape == 14 && tempo.getSortie())
  {
    etape_futur = 16;
  }

  if (etape == 15 && tempo.getSortie())
  {
    etape_futur = 13;
  }

  if (etape == 15 && !sensor_low && !sensor_high)
  {
    etape_futur = 14;
  }

  if (etape == 16 && sensor_high)
  {
    etape_futur = 17;
  }

  if (etape == 16 && tempo.getSortie())
  {
    etape_futur = 18;
  }

  if (etape == 17 && tempo.getSortie())
  {
    etape_futur = 15;
  }

  if (etape == 17 && !sensor_low && !sensor_high)
  {
    etape_futur = 16;
  }

  if (etape == 18 && sensor_high)
  {
    etape_futur = 19;
  }

  if (etape == 19 && tempo.getSortie())
  {
    etape_futur = 17;
  }

  if (etape == 19 && !sensor_low && !sensor_high)
  {
    etape_futur = 18;
  }

  /* Fin de mode automatique */

  if (etape != etape_futur)
  {
    etape = etape_futur;
  }
}

void Actions()
{
  digitalWrite(OUT_DISPLAY_GRAFCET, etape);
  digitalWrite(OUT_DISPLAY_MODE, etape >= 10);
  digitalWrite(OUT_PUMP_1, !sensor_max && (pompe1 == 1 || etape >= 12));
  digitalWrite(OUT_PUMP_2, !sensor_max && (pompe2 == 1 || etape >= 14));
  digitalWrite(OUT_PUMP_3, !sensor_max && (pompe3 == 1 || etape >= 16));
  digitalWrite(OUT_PUMP_4, !sensor_max && (pompe4 == 1 || etape >= 18));
  digitalWrite(OUT_BEEP, etape == 1);

  if (etape >= 11)
  {
    tempo.activation();
  }
}
